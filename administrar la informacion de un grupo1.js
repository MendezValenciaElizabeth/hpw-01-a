alumno=[
    {"num.control":"1973842","nombrec":"juan","apellidos": "mendez perez","grupo":"ISA"},
    {"num.control":"7630173","nombrec":"horacio", "apellidos":"sanchez sanchez","grupo":"ISA"},
    {"num.control":"2637328","nombrec":"gerardo", "apellidos": "gutierrez lopez","grupo":"ISA"},
    {"num.control":"7643321","nombrec":"carlos", "apellidos":"perez perez","grupo":"ISA"},
    {"num.control":"8743721","nombrec":"andres", "apellidos": "lopez juarez","grupo":"ISA"},
    {"num.control":"3840207","nombrec":"jesus", "apellidos": "luis rosales","grupo":"ISA"},
    {"num.control":"6540009","nombrec":"oscar", "apellidos": "lopez luna","grupo":"ISA"},
    {"num.control":"5436772","nombrec":"rosario", "apellidos": "hurtado zarate","grupo":"ISA"},
    {"num.control":"9990432","nombrec":"maria", "apellidos": "hernandez martinez","grupo":"ISA"},
    {"num.control":"7954821","nombrec":"mayra", "apellidos":"duarte mendez","grupo":"ISA"}
];

materia=[
    {"clave":"his74","nombre":"historia"},
    {"clave":"cie63","nombre":"ciencias naturales"},
    {"clave":"fis08","nombre":"fisica general"},
    {"clave":"mat56","nombre":"matematicas"},
    {"clave":"esp12","nombre":"español"},
    {"clave":"qui77","nombre":"quimica general"},
    {"clave":"geo99","nombre":"geografia"}
];

docente=[
    {"clave":"p77392","nombrep":"luis", "apellidos":"rosales ruiz"},
    {"clave":"p12345","nombrep":"gilberto", "apellidos": "lopez cortes"},
    {"clave":"p58301","nombrep":"laura", "apellidos": "juarez figueroa"},
    {"clave":"p43800","nombrep":"juan", "apellidos": "morales jimenez"},
    {"clave":"p53781","nombrep":"hector", "apellidos": "hernandez lopez"},
    {"clave":"p76548","nombrep":"jesus", "apellidos": "almaraz sanchez"},
    {"clave":"p68432","nombrep":"jorge", "apellidos": "bautista juarez"}
];

grupo={
    "clave":"473993",
    "nombre":"ISA",
    "alumnos":alumno,
    "materias":materia,
    "docentes":docente,
};

/*funcion que dado un grupo, imprima una lista de alumnos*/
function imprimir_lista_alumnos(obj_grupo)
{
    for(var i=0;i<obj_grupo["alumnos"].length; i++)
    {
        console.log(obj_grupo["alumnos"][i]["nombrec"]);
    }
}

console.log(imprimir_lista_alumnos(grupo));

/*funcion buscar si alumno esta en el grupo*/
function imprimir_alumno(grupo,numc)
{
    var existe=false;
    for(var i=0; i<grupo["alumnos"].length;i++)
    {
        if(numc==grupo["alumnos"][i]["num.control"])
        {
            existe=true;
        }
    }
    return existe;
}

/*funcion buscar si un docente esta en el grupo*/
function imprimir_docente(grupo,numc)
{
    var existe=false;
    for(var i=0; i<grupo["docentes"].length;i++)
    {
        if(numc==grupo["docentes"][i]["clave"])
        {
            existe=true;
        }
    }
    return existe;
}

/*funcion buscar si materia esta en el grupo*/
function imprimir_materia(grupo,numm)
{
    var existe=false;
    for(var i=0; i<grupo["materias"].length;i++)
    {
        if(numm==grupo["materias"][i]["clave"])
        {
            existe=true;
        }
    }
    return existe;
}

console.log(imprimir_alumno(grupo,"2637328"));

/*insertar nuevo alumno*/

function ingresar_nuevo_alumno(grupo,nconn,nombn,grupn)
{
    var existe;
   if(imprimir_alumno(grupo,nconn)===false)
   {
       alumno.push({"num.control":nconn,"nombrec":nombn,"grupo":grupn});
       existe=true;
   }
   else
   {
      existe=false;
   }
     return existe;
}

console.log(ingresar_nuevo_alumno(grupo,"8657492","javier sanchez","1111"));


/*insertar nuevo docente*/

function ingresar_nuevo_docente(grupo,clavep,nombrep)
{
    var existe;
   if(imprimir_docente(grupo,clavep)===false)
   {
       docente.push({"clave":clavep,"nombrep":nombrep});
       existe=true;
   }
   else
   {
      existe=false;
   }
     return existe;
}

console.log(ingresar_nuevo_docente(grupo,"p006523","jacinto martinez"));

/*insertar nueva materia*/

function ingresar_nueva_materia(grupo,clavem,nombrem)
{
    var existe;
   if(imprimir_materia(grupo,clavem)===false)
   {
       materia.push({"clave":clavem,"nombre":nombrem});
       existe=true;
   }
   else
   {
      existe=false;
   }
     return existe;
}

console.log(ingresar_nueva_materia(grupo,"edu75492","educacion fisica"));

function eliminar_alumno(grupo,nconn,nombn,grupn)
{
    var existe;
   if(imprimir_alumno(grupo,nconn)===true)
   {
      alumno.splice(0,1,2,{"num.control":nconn,"nombrec":nombn,"grupo":grupn}); 
       existe=false;
   }
   else
   {
}
 return existe;
}

console.log(eliminar_alumno(grupo,"8657492","javier sanchez","1111"));
